package br.com.itau.haratinho.investi.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.itau.haratinho.investi.model.Produto;
import br.com.itau.haratinho.investi.repository.ProdutoRepository;
import br.com.itau.haratinho.investi.service.ProdutoService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { ProdutoService.class })
public class ProdutoServiceTest {

	@Autowired
	ProdutoService produtoService;

	@MockBean
	ProdutoRepository produtoRepository;

	@Test
	public void deveListarTodosOsProdutos() {
		// Setup
		Produto produto = new Produto();

		List<Produto> produtos = new ArrayList<>();
		produtos.add(produto);

		when(produtoRepository.findAll()).thenReturn(produtos);

		// Action
		Iterable<Produto> resultado = produtoService.obterProdutos();

		// Check
		Iterator<Produto> iterador = resultado.iterator();
		Produto produtoResultado = iterador.next();

		assertNotNull(produtoResultado);

	}

	@Test
	public void deveCriarUmProduto() {
		// setup
		Produto produto = new Produto();

		// action
		produtoService.criarProduto(produto);

		// check
		verify(produtoRepository).save(produto);
	}

	@Test
	public void deveEditarUmProduto() {
		// setup
		long id = 1L;
		String novoNome = ("Novo Fundo");
		double novoRendimento = (0.05);
		Produto produtoDoBanco = criarProduto(id);
		Produto produtoDoFront = criarProduto(id);
		produtoDoFront.setNome(novoNome);
		produtoDoFront.setRendimento(novoRendimento);

		when(produtoRepository.findById(id)).thenReturn(Optional.of(produtoDoBanco));
		when(produtoRepository.save(produtoDoBanco)).thenReturn(produtoDoBanco);

		// action
		Produto resultado = produtoService.editarProduto(id, produtoDoFront);

		// check
		assertNotNull(resultado);
		assertEquals(novoNome, resultado.getNome());

	}

	private Produto criarProduto(long id) {
		Produto produto = new Produto();
		produto.setId(id);
		produto.setNome("Fundo de Investimento");
		produto.setRendimento(0.05);

		return produto;
	}

	@Test
	public void deveApagarUmProduto() {
		// setup
		long id = 1L;
		Produto produtoDoBanco = criarProduto(id);

		when(produtoRepository.findById(id)).thenReturn(Optional.of(produtoDoBanco));

		// action
		produtoService.apagarProduto(id);

		// check
		verify(produtoRepository).delete(produtoDoBanco);

	}

}
