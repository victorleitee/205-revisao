package br.com.itau.haratinho.investi.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.haratinho.investi.model.Cliente;
import br.com.itau.haratinho.investi.repository.ClienteRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { ClienteService.class })
public class ClienteServiceTest {

	@Autowired
	ClienteService clienteService;

	@MockBean
	ClienteRepository clienteRepository;

	@Test
	public void deveListarTodosOsClientes() {
		// setup
		Cliente cliente = new Cliente();

		List<Cliente> clientes = new ArrayList<>();
		clientes.add(cliente);

		when(clienteRepository.findAll()).thenReturn(clientes);

		// action
		List<Cliente> resultado = Lists.newArrayList(clienteService.obterClientes());

		// check
		assertNotNull(resultado.get(0));
		assertEquals(cliente, resultado.get(0));
	}

	@Test
	public void deveCriarUmCliente() {
		// setup
		Cliente cliente = new Cliente();

		// action
		clienteService.criarCliente(cliente);

		// check
		verify(clienteRepository).save(cliente);
	}

	@Test
	public void deveEditarUmCliente() {
		// setup
		Long id = 1L;
		String novoNome = "Almeida Carlos";
		Cliente clienteDoBanco = criarCliente(id);

		Cliente clienteDoFront = criarCliente(id);
		clienteDoFront.setNome(novoNome);

		when(clienteRepository.findById(id)).thenReturn(Optional.of(clienteDoBanco));
		when(clienteRepository.save(clienteDoBanco)).thenReturn(clienteDoBanco);

		// action
		Cliente resultado = clienteService.editarCliente(id, clienteDoFront);

		// check
		assertNotNull(resultado);
		verify(clienteRepository).save(clienteDoBanco);
		assertEquals(novoNome, resultado.getNome());
		assertEquals(clienteDoBanco.getNome(), clienteDoFront.getNome());
	}

	private Cliente criarCliente(long id) {
		Cliente cliente = new Cliente();
		cliente.setId(id);
		cliente.setNome("João de Andrade");
		cliente.setCpf("426.095.960-30");

		return cliente;
	}

	@Test(expected = ResponseStatusException.class)
	public void deveLancarExcecaoQuandoNaoEncontrarOCliente() {
		// setup
		Long id = 1L;
		Long novoId = 10L;
		Cliente clienteDoFront = criarCliente(novoId);

		when(clienteRepository.findById(id)).thenReturn(Optional.empty());

		// action
		clienteService.editarCliente(id, clienteDoFront);
	}

}
