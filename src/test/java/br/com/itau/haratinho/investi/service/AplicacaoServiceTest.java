package br.com.itau.haratinho.investi.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.haratinho.investi.model.Aplicacao;
import br.com.itau.haratinho.investi.model.AplicacaoDTO;
import br.com.itau.haratinho.investi.model.Cliente;
import br.com.itau.haratinho.investi.model.Produto;
import br.com.itau.haratinho.investi.repository.AplicacaoRepository;
import br.com.itau.haratinho.investi.repository.ClienteRepository;
import br.com.itau.haratinho.investi.repository.ProdutoRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { AplicacaoService.class })
public class AplicacaoServiceTest {

	@Autowired
	private AplicacaoService aplicacaoService;

	@MockBean
	ClienteRepository clienteRepository;

	@MockBean
	ProdutoRepository produtoRepository;

	@MockBean
	AplicacaoRepository aplicacaoRepository;

	@Test
	public void deveListarAsAplicacaoesDeUmCliente() {
		// setup
		long idCliente = 1;

		AplicacaoDTO aplicacaoDTO = mock(AplicacaoDTO.class);
		List<AplicacaoDTO> aplicacoes = new ArrayList<>();
		aplicacoes.add(aplicacaoDTO);

//		when(aplicacaoRepository.findAllByCliente_IdCliente(idCliente))
//		.thenReturn(aplicacoes);

		when(aplicacaoRepository.findSummary(idCliente)).thenReturn(aplicacaoDTO);

		// action
		List<Aplicacao> resultado = Lists.newArrayList(aplicacaoService.obterAplicacoesCliente(idCliente));

		// check
		assertEquals(aplicacaoDTO, resultado.get(0));
	}

	@Test
	public void deveCriarUmaAplicacao() {
		// setup
		Cliente cliente = new Cliente();
		cliente.setId(1L);

		Produto produto = new Produto();
		produto.setId(1L);

		Aplicacao aplicacao = new Aplicacao();
		aplicacao.setMeses(10);
		aplicacao.setValor(1000);
		aplicacao.setCliente(cliente);
		aplicacao.setProduto(produto);

		when(clienteRepository.findById(cliente.getId())).thenReturn(Optional.of(cliente));

		when(produtoRepository.findById(produto.getId())).thenReturn(Optional.of(produto));

		// action
		Aplicacao resultado = aplicacaoService.criarAplicacao(cliente.getId(), aplicacao, produto);

		// check
		verify(aplicacaoRepository).save(aplicacao);
		assertNotNull(resultado);
		assertEquals(resultado, aplicacao);
		assertEquals(resultado.getIdAplicacao(), aplicacao.getIdAplicacao());
	}

	@Test(expected = ResponseStatusException.class)
	public void deveLancarExcecaoQuandoNaoEncontrarOCliente() {
		// setup
		Long id = 1L;
		Long novoId = 10L;

		Cliente cliente = criarCliente(novoId);

		Aplicacao aplicacao = new Aplicacao();
		aplicacao.setMeses(10);
		aplicacao.setValor(1000);
		aplicacao.setCliente(cliente);
		
		Produto produto = new Produto();

		when(clienteRepository.findById(id)).thenReturn(Optional.empty());

		// action
		Aplicacao resultado = aplicacaoService.criarAplicacao(cliente.getId(), aplicacao, produto);
	}

	@Test(expected = ResponseStatusException.class)
	public void deveLancarExcecaoQuandoNaoEncontrarOProduto() {
		// setup
		Long id = 1L;
		Long novoId = 10L;

		Cliente cliente = criarCliente(novoId);

		Aplicacao aplicacao = new Aplicacao();
		aplicacao.setMeses(10);
		aplicacao.setValor(1000);
		aplicacao.setCliente(cliente);

		when(clienteRepository.findById(cliente.getId())).thenReturn(Optional.of(cliente));

		when(produtoRepository.findById(id)).thenReturn(Optional.empty());
		
		Produto produto = new Produto();

		// action
		Aplicacao resultado = aplicacaoService.criarAplicacao(cliente.getId(), aplicacao, produto);
	}

	private Cliente criarCliente(long id) {
		Cliente cliente = new Cliente();
		cliente.setId(id);
		cliente.setNome("João de Andrade");
		cliente.setCpf("426.095.960-30");

		return cliente;
	}

}
