package br.com.itau.haratinho.investi.service;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.itau.haratinho.investi.model.Produto;
import br.com.itau.haratinho.investi.model.Simulacao;
import br.com.itau.haratinho.investi.repository.ProdutoRepository;
import br.com.itau.haratinho.investi.repository.SimulacaoRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { SimulacaoService.class })
public class SimulacaoServiceTest {

	@Autowired
	SimulacaoService simulacaoService;

	@MockBean
	SimulacaoRepository simulacaoRepository;

	@MockBean
	ProdutoRepository produtoRepository;

	@Test
	public void deveSimularUmaAplicacao() {
		// setup
		long id = 1L;
		Simulacao simulacao = new Simulacao();
		simulacao.setMeses(24);
		simulacao.setValor(1000);
		Produto produto = criarProduto(id);
		simulacao.setProduto(produto);
		when(produtoRepository.findById(id)).thenReturn(Optional.of(produto));

		// action
		simulacaoService.simularAplicacao(simulacao);

		// check
		verify(simulacaoRepository).save(simulacao);
	}

	private Produto criarProduto(long id) {
		Produto produto = new Produto();
		produto.setId(id);
		produto.setNome("Fundo de Investimento");
		produto.setRendimento(0.05);

		return produto;
	}
}
