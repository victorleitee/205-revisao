package br.com.itau.haratinho.investi.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.itau.haratinho.investi.model.Aplicacao;
import br.com.itau.haratinho.investi.model.AplicacaoDTO;

public interface AplicacaoRepository extends CrudRepository<Aplicacao, Long> {

	@Query("SELECT a FROM Aplicacao a WHERE cliente_id_cliente = :cliente_id_cliente")
	AplicacaoDTO findSummary(@Param("cliente_id_cliente") Long idCliente);

	Iterable<Aplicacao> findAllByCliente_IdCliente(Long idCliente);
}
