package br.com.itau.haratinho.investi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.haratinho.investi.model.Simulacao;
import br.com.itau.haratinho.investi.model.SimulacaoDTO;
import br.com.itau.haratinho.investi.service.SimulacaoService;

@RestController
public class SimulacaoController {

	@Autowired
	private SimulacaoService simulacaoService;

	@PostMapping("/simulacao")
	@ResponseStatus(code = HttpStatus.CREATED)
	public SimulacaoDTO criarSimulacao(@RequestBody Simulacao simulacao) {		
		return simulacaoService.simularAplicacao(simulacao);
	}
}
