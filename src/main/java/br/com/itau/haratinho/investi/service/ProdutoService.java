package br.com.itau.haratinho.investi.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.haratinho.investi.model.Produto;
import br.com.itau.haratinho.investi.repository.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;

	public Iterable<Produto> obterProdutos() {
		return produtoRepository.findAll();
	}

	public void criarProduto(Produto produto) {
		produtoRepository.save(produto);
	}

	public void apagarProduto(Long id) {
		Produto produto = encontraOuDaErro(id);
		produtoRepository.delete(produto);
	}

	public Produto editarProduto(Long id, Produto produtoAtualizado) {
		Produto produto = encontraOuDaErro(id);
		
		produto.setNome(produtoAtualizado.getNome());
		produto.setRendimento(produtoAtualizado.getRendimento());		
		return produtoRepository.save(produto);
	}
	
	public Produto encontraOuDaErro(Long id) {
		Optional<Produto> optional = produtoRepository.findById(id);

		if (!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Produto não encontrado");
		}

		return optional.get();
	}
}

//package br.com.itau.haratinho.investi.controller;
//
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PatchMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.ResponseStatus;
//import org.springframework.web.bind.annotation.RestController;
//
//import br.com.itau.haratinho.investi.model.Produto;
//
//@RestController
//public class ProdutoController {
//
//	@GetMapping("/produto")
//	public Iterable<Produto> listarProdutos() {
//		return produtoService.obterProdutos();
//	}
//
//	@PostMapping("/produto")
//	@ResponseStatus(code = HttpStatus.CREATED)
//	public void criarProduto(@RequestBody Produto produto) {
//		produtoService.criarProduto(produto);
//	}
//
//	@DeleteMapping("/produto/{id}")
//	@ResponseStatus(code = HttpStatus.NO_CONTENT)
//	public void apagarProduto(@PathVariable Long id) {
//		produtoService.apagarProduto(id);
//	}
//
//	@PatchMapping("/produto/{id}")
//	public Produto editarProduto(@PathVariable Long id, @RequestBody Produto produto) {
//		return produtoService.editarProduto(id, produto);
//	}
//
//}