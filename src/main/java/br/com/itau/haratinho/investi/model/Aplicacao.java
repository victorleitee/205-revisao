package br.com.itau.haratinho.investi.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "aplicacao")
public class Aplicacao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idAplicacao;

	@Column
	private double valor;

	@Column
	private long meses;

	@ManyToOne(cascade = CascadeType.MERGE)
	private Produto produto;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.MERGE)
	private Cliente cliente;

	public long getIdAplicacao() {
		return idAplicacao;
	}

	public void setIdAplicacao(long id) {
		this.idAplicacao = id;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public long getMeses() {
		return meses;
	}

	public void setMeses(long meses) {
		this.meses = meses;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}
