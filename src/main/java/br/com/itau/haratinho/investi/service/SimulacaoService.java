package br.com.itau.haratinho.investi.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.haratinho.investi.model.Produto;
import br.com.itau.haratinho.investi.model.Simulacao;
import br.com.itau.haratinho.investi.model.SimulacaoDTO;
import br.com.itau.haratinho.investi.repository.ProdutoRepository;
import br.com.itau.haratinho.investi.repository.SimulacaoRepository;

@Service
public class SimulacaoService {

	@Autowired
	private SimulacaoRepository simulacaoRepository;

	@Autowired
	private ProdutoRepository produtoRepository;

	public SimulacaoDTO simularAplicacao(Simulacao simulacao) {
//		double valorFinal;
		Produto produto = encontraOuDaErro(simulacao.getProduto().getId());

		simulacao.setValor(Math.pow((produto.getRendimento() + 1), simulacao.getMeses()) * simulacao.getValor());
		simulacaoRepository.save(simulacao);
		
		return simulacaoRepository.findSummary(simulacao.getId());
	}

	public Produto encontraOuDaErro(Long id) {
		Optional<Produto> optional = produtoRepository.findById(id);

		if (!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Produto não encontrado");
		}

		return optional.get();
	}

}
