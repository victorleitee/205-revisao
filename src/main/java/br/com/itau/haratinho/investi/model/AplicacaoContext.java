package br.com.itau.haratinho.investi.model;

public class AplicacaoContext {
	private Aplicacao aplicacao;
	private Produto produto;

	public Aplicacao getAplicacao() {
		return aplicacao;
	}

	public void setAplicacao(Aplicacao aplicacao) {
		this.aplicacao = aplicacao;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

}
