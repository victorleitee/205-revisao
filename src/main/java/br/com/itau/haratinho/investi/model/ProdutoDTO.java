package br.com.itau.haratinho.investi.model;

public interface ProdutoDTO {
	
	Long getId();

	String getNome();

	Double getRendimento();

}
