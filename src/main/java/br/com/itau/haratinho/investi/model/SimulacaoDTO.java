package br.com.itau.haratinho.investi.model;

public interface SimulacaoDTO {
	
	Long getIdSimulacao();
	
	Integer getMeses();
	
	Double getValorFinal();
	
	Long getIdProduto();

}
