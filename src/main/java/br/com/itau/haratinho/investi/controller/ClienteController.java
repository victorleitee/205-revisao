package br.com.itau.haratinho.investi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.haratinho.investi.model.Cliente;
import br.com.itau.haratinho.investi.service.ClienteService;

@RestController
public class ClienteController {

	@Autowired
	private ClienteService clienteService;
	
	@GetMapping("/cliente")
	public Iterable<Cliente> listarClientes() {
		return clienteService.obterClientes();
	}
	
	@PostMapping("/cliente")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Cliente criarCliente(@RequestBody Cliente cliente) {
		return clienteService.criarCliente(cliente);
	}
	
	
	@PatchMapping("/cliente/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Cliente editarCliente(@PathVariable Long id, @RequestBody Cliente cliente) {
		return clienteService.editarCliente(id, cliente);
	}
	
	
	@DeleteMapping("/cliente/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void apagarCliente(@PathVariable Long id) {
		clienteService.apagarCliente(id);
	}
	
}
