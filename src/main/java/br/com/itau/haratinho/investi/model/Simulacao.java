	package br.com.itau.haratinho.investi.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "simulacao")
public class Simulacao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idSimulacao;

	@Column
	private int meses;

	@Column
	private double valorFinal;

	@OneToOne(cascade = CascadeType.MERGE)
	private Produto produto;

	public Long getId() {
		return idSimulacao;
	}

	public void setId(Long id) {
		this.idSimulacao = id;
	}

	public int getMeses() {
		return meses;
	}

	public void setMeses(int meses) {
		this.meses = meses;
	}

	public double getValor() {
		return valorFinal;
	}

	public void setValor(double valor) {
		this.valorFinal = valor;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	public long getIdProduto() {
		return this.produto.getId();
	}

}
