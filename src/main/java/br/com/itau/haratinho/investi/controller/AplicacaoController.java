package br.com.itau.haratinho.investi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.haratinho.investi.model.Aplicacao;
import br.com.itau.haratinho.investi.model.AplicacaoContext;
import br.com.itau.haratinho.investi.model.AplicacaoDTO;
import br.com.itau.haratinho.investi.service.AplicacaoService;

@RestController
public class AplicacaoController {
	
	@Autowired
	private AplicacaoService aplicacaoService;

	@GetMapping("/cliente/{id}/aplicacao")
	public Iterable<Aplicacao> listarAplicacoesCliente(@PathVariable Long id) {
		return aplicacaoService.obterAplicacoesCliente(id);
	}
	//plicacao aplicacao, Produto produto
	@PostMapping("/cliente/{id}/aplicacao")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Aplicacao criarAplicacao(@PathVariable Long id, @RequestBody AplicacaoContext aplicacaoContext) {
		return aplicacaoService.criarAplicacao(id, aplicacaoContext.getAplicacao(), aplicacaoContext.getProduto());
	}

}
