package br.com.itau.haratinho.investi.model;

import java.util.List;

public interface AplicacaoDTO {
	
	Long getIdAplicacao();
	
	Double getValor();
	
	Long getMeses();
		
	List<Produto> getProduto();

}
