package br.com.itau.haratinho.investi.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.haratinho.investi.model.Cliente;
import br.com.itau.haratinho.investi.repository.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	// get
	public Iterable<Cliente> obterClientes() {
		return clienteRepository.findAll();
	}

	// post
	public Cliente criarCliente(Cliente cliente) {

		clienteRepository.save(cliente);
		return cliente;
	}

	// patch
	public Cliente editarCliente(Long id, Cliente clienteAtualizado) {

		Cliente cliente = encontraOuDaErro(id);

		cliente.setNome(clienteAtualizado.getNome());

		return clienteRepository.save(cliente);
	}

	public Cliente encontraOuDaErro(Long id) {
		Optional<Cliente> optional = clienteRepository.findById(id);

		if (!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cliente não encontrado");
		}

		return optional.get();
	}

	// delete
	public void apagarCliente(Long id) {
		Cliente cliente = encontraOuDaErro(id);
		clienteRepository.delete(cliente);
	}

}
