package br.com.itau.haratinho.investi.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.haratinho.investi.model.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Long>{

}
