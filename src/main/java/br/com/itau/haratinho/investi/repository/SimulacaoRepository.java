package br.com.itau.haratinho.investi.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.itau.haratinho.investi.model.Simulacao;
import br.com.itau.haratinho.investi.model.SimulacaoDTO;

@Repository
public interface SimulacaoRepository extends CrudRepository<Simulacao, Long> {
	@Query("SELECT s FROM Simulacao s WHERE id = :id")
	SimulacaoDTO findSummary(@Param("id") Long id);
}