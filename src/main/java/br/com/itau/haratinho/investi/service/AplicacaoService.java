package br.com.itau.haratinho.investi.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.haratinho.investi.model.Aplicacao;
import br.com.itau.haratinho.investi.model.Cliente;
import br.com.itau.haratinho.investi.model.Produto;
import br.com.itau.haratinho.investi.repository.AplicacaoRepository;
import br.com.itau.haratinho.investi.repository.ClienteRepository;
import br.com.itau.haratinho.investi.repository.ProdutoRepository;

@Service
public class AplicacaoService {

	@Autowired
	private AplicacaoRepository aplicacaoRepository;

	@Autowired
	private ProdutoRepository produtoRepository;

	@Autowired
	private ClienteRepository clienteRepository;

	// get aplicacao de cliente
	public Iterable<Aplicacao> obterAplicacoesCliente(Long idCliente) {
//		return aplicacaoRepository.findSummary(idCliente);
		return aplicacaoRepository.findAllByCliente_IdCliente(idCliente);
	}

	public Aplicacao criarAplicacao(Long idCliente, Aplicacao aplicacao, Produto produtoEntrada) {
		Cliente cliente = encontraOuDaErroCliente(idCliente);
		Produto produto = encontraOuDaErroProduto(produtoEntrada.getId());

		aplicacao.setCliente(cliente);
		aplicacao.setProduto(produto);
		aplicacaoRepository.save(aplicacao);

//		AplicacaoDTO aplicacaoDTO = aplicacaoRepository.findSummaryAplicacao(aplicacao.getId());

		return aplicacao;

	}

	public Cliente encontraOuDaErroCliente(Long id) {
		Optional<Cliente> optional = clienteRepository.findById(id);

		if (!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cliente não encontrado");
		}

		return optional.get();
	}

	public Produto encontraOuDaErroProduto(Long id) {
		Optional<Produto> optional = produtoRepository.findById(id);

		if (!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Produto não encontrado");
		}

		return optional.get();
	}

}
